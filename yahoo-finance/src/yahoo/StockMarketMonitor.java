package yahoo;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import yahoo.domain.Stock;
import yahoo.web.YahooException;
import yahoo.web.YahooStockReader;

public class StockMarketMonitor {

	private List<Stock> currentStocks = new ArrayList<>();
	private List<YahooStockReader> readers = new ArrayList<>();
	private List<Stock> oldStocks = new ArrayList<>();
	private static Logger log = Logger.getLogger(StockMarketMonitor.class.getName());

	public StockMarketMonitor() {
		// company.add("AAPL"); // YNDX,GAZP,SBER ORCL

		// newCompany(new YahooStockReader("ORCL"));

	}

	public void saveOldStocks() {
		oldStocks.clear();

		for (Stock stock : currentStocks)

			oldStocks.add(stock);
	}

	public void newCompany(YahooStockReader comp) throws YahooException {

		readers.add(comp);
	//	oldStocks.clear();
		//currentStocks.clear();
		currentStocks.add(comp.getQuote());
		log.info("��������� �������� "+ comp.getSymbol());
	}

	public String refresh() throws YahooException {
		saveOldStocks();
		currentStocks.clear();
		for (YahooStockReader comp : readers) {
			currentStocks.add(comp.getQuote());
		}
		log.info("������ ���������");
		return printData();
		
	}

	public String printData() {

		OutputData out = new OutputData(currentStocks, oldStocks);
		return out.print();
	}

	public int getOldStocksSize() {
		return oldStocks.size();
	}

	public int getCurrentStocksSize() {

		return currentStocks.size();
	}

	public int getReadersSize() {
		return readers.size();
	}

}
