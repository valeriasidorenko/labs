package yahoo;
import yahoo.StockMarketGui;
import yahoo.StockMarketMonitor;
import yahoo.web.YahooException;
import yahoo.web.YahooStockReader;
public class ControllerStockMarket {

	StockMarketGui smg;
	StockMarketMonitor smm;
	
	public ControllerStockMarket(StockMarketGui smg,StockMarketMonitor smm){
		this.smg=smg;
		this.smm=smm;
		smg.addController(this);	
	}
	
	public void newComp(String comp) throws YahooException{
	
		smm.newCompany(new YahooStockReader(comp)); 

		
	}
	public void forRefresh() throws YahooException {
		smg.newVal(smm.refresh());
	}
}
