package yahoo;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import yahoo.ControllerStockMarket;
import yahoo.web.YahooException;
import yahoo.web.YahooStockReader;

public class StockMarketGui {

	// private final static String[] STOCK_SYMBOLS = { "AAPL", "GOOG", "ORCL" };

	private StockMarketMonitor monitor;
	ControllerStockMarket csm;

	private static Logger log = Logger.getLogger(StockMarketMonitor.class.getName());

	public StockMarketGui() {
		// monitor = new StockMarketMonitor();
	}

	public void addController(ControllerStockMarket csm) {
		this.csm = csm;
	}

	public void newVal(String result) {
		System.out.println(result);
	}

	public void start() throws YahooException {
		Scanner sc = new Scanner(System.in);
		String input = "";
		try {
			do {
				System.out.println("�������� ��������:");
				System.out.println("r - �������� ������");
				System.out.println("n - �������� ��������");
				System.out.println("x - �����");
				input = sc.nextLine();
				if (input.equalsIgnoreCase("r")) {
					csm.forRefresh();

				}

				if (input.equalsIgnoreCase("n")) {

					System.out.println("������� ��������");
					String comp = sc.next();
					csm.newComp(comp);
					csm.forRefresh();

				}
			} while (!input.equalsIgnoreCase("x"));
		} catch (YahooException e) {
			System.out.println(e.getMessage());
			log.log(Level.SEVERE, "Exception:", e);
		}
		sc.close();
	}

}
