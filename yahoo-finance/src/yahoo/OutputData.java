package yahoo;

import java.util.List;
import java.util.logging.Logger;
import java.util.ArrayList;
import yahoo.domain.Stock;

public class OutputData {

	private Calculation calc;
	List<Stock> newSt = new ArrayList<>();
	List<Stock> oldSt = new ArrayList<>();

	public OutputData(List<Stock> newSt, List<Stock> oldSt) {
		calc = new Calculation();
		this.newSt = newSt;
		this.oldSt = oldSt;
	}

	public String print() {
		double differTheLast = 0.00;
		double differTheOpen = 0.00;
		String s="";
		for (Stock st : newSt) {

			if (oldSt.isEmpty()) {
				s=st + " " + st.getLastTrade() + " " + st.getOpen();
			}

			else {
				differTheLast = ((st.getLastTrade()) * 100 / oldSt.get(newSt.indexOf(st)).getLastTrade()) - 100;
				differTheOpen = ((st.getLastTrade() * 100) / (st.getOpen())) - 100;

			s=st.getSymbol() + " " + oldSt.get(newSt.indexOf(st)).getLastTrade() + " "
						+ calc.returnSymbols(differTheLast) + " " + calc.str(differTheLast) + "% " + st.getLastTrade() + " "
						+ calc.returnSymbols(differTheOpen) + " " + calc.str(differTheOpen) + "% " + st.getOpen();
			}

		}
	
		return s;
		
	

	}





}
