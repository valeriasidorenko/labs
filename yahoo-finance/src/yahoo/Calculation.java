package yahoo;

import java.text.DecimalFormat;

public class Calculation {
	
	public Calculation(){
		
	}
	
	
	public String str(double diff){
	//	String percent=String.format("%.2f", diff);
		DecimalFormat df=new DecimalFormat("##.##");
		String percent=df.format(Math.abs(diff));
		return percent;
	}
	
	public String returnSymbols(double diff) {
		String symbol = "";
		String solut = "";

		if (diff > 0) {
			symbol = "+";
		} else {
			symbol = "-";
		}

		if (Math.abs(diff) < 0.2) {
			solut = numberOfSign(symbol, 1);
		}
		if (Math.abs(diff) >= 0.2 && Math.abs(diff) < 1) {
			solut = numberOfSign(symbol, 2);
		}
		if (Math.abs(diff) >= 1 && Math.abs(diff) < 2) {
			solut = numberOfSign(symbol, 3);
		}
		if (Math.abs(diff) >= 2 && Math.abs(diff) < 5) {
			solut = numberOfSign(symbol, 4);
		}
		if (Math.abs(diff) >= 5) {
			solut = numberOfSign(symbol, 5);
		}
		return solut;
	}
	
	public String numberOfSign(String symb, int count) {
		String sign = "";
		for (int i = 0; i < count; i++) {
			sign += symb;
		}
		return sign;
	}

}
