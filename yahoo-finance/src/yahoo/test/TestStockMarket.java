package yahoo.test;



import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;
import yahoo.StockMarketMonitor;
import yahoo.domain.Stock;
import yahoo.web.YahooException;
import yahoo.web.YahooStockReader;


public class TestStockMarket {

	private StockMarketMonitor mark;
	private YahooStockReader ya;

	@Before
	public void first() throws YahooException {

		ya = mock(YahooStockReader.class);
		when(ya.getSymbol()).thenReturn("ORCL");
		when(ya.getQuote()).thenReturn(new Stock("ORCL", 44.58, 44.61));
		mark = new StockMarketMonitor();
		 //mark.newCompany(ya);
	}

	@Test
	public void testSaveOldStocks() {
		mark.saveOldStocks();
		assertEquals(0, mark.getOldStocksSize());
	}
	
	@Test 
	public void testSaveOldStockstwo() throws YahooException{
		mark.newCompany(ya);
		mark.refresh();
		mark.refresh();
		assertEquals(1,mark.getOldStocksSize());

	}

	@Test
	public void testRefresh() throws YahooException {
		mark.newCompany(ya);
		mark.refresh();
		assertEquals(1, mark.getCurrentStocksSize());
		

	}

	@Test
	public void testNewCompany() throws YahooException {
		
		mark.newCompany(ya);
		assertEquals(1, mark.getReadersSize());

	}
	
	
}
