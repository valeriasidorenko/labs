package yahoo.test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import yahoo.OutputData;
import yahoo.domain.Stock;

public class TestOutputData {

	private OutputData outDat;
	
	@Before
	public void first(){
		List<Stock> newSt = new ArrayList<>();
		List<Stock> oldSt = new ArrayList<>();
		newSt.add(new Stock("ORCL", 44.13, 44.33));
		oldSt.add(new Stock("ORCL", 44.12, 44.33));	
		outDat = new OutputData(newSt, oldSt);
	}
	
	
	@Test
	public void testPrint(){

		assertEquals("ORCL 44.12 + 0,02% 44.13 -- 0,45% 44.33", outDat.print());
		
		
	}
	
}
