package yahoo.test;

import yahoo.ControllerStockMarket;
import yahoo.StockMarketGui;
import yahoo.StockMarketMonitor;
import yahoo.web.YahooException;
import static org.mockito.Mockito.verify;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import org.mockito.Mockito;

public class TestControllerStockMarket {

	private ControllerStockMarket contr;
	private StockMarketMonitor smm;
	private StockMarketGui smg;

	@Before
	public void first() {
		smm = mock(StockMarketMonitor.class);
		smg = mock(StockMarketGui.class);
		contr = new ControllerStockMarket(smg, smm);
	}

	@Test
	public void testNewComp() throws YahooException {
		contr.newComp("ORCL");
		verify(smm).newCompany(Mockito.any());
	}

	@Test
	public void testForRefresh() throws YahooException {
		contr.newComp("ORCL");
		contr.forRefresh();
		verify(smm).refresh();
		verify(smg).newVal(Mockito.anyString());

	}

}
