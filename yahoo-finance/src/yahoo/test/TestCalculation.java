package yahoo.test;

import yahoo.Calculation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestCalculation {

	private Calculation calc;
	
	@Before	
	public void first(){
		calc = new Calculation();
	}
	
	@Test
	public void testStr(){
	assertEquals("0,56",calc.str(0.555));
	}
	
	@Test
	public void testNumberOfSign(){
		assertEquals("--",calc.numberOfSign("-",2));
	}
	
	@Test
	public void testNumberOfSign�Two(){
		assertEquals("+++",calc.numberOfSign("+",3));
	}
	
	@Test
	public void testReturnSymbols(){//�������� ��� ������� ��� ������ ����������++
		assertEquals("++",calc.returnSymbols(0.4));
		assertEquals("-",calc.returnSymbols(-0.1));
	}
	
	@Test
	public void testReturnSymbolsTwo(){
		assertEquals("+++++",calc.returnSymbols(6));
		assertEquals("----",calc.returnSymbols(-3));
	}
	@Test
	public void testReturnSymbolsThree(){//��������� ��������
		assertEquals("+++",calc.returnSymbols(1));
		assertEquals("--",calc.returnSymbols(-0.2));
	}

	
}
