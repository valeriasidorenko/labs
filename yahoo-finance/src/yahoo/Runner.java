package yahoo;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import yahoo.web.YahooException;
//import com.sun.istack.internal.logging.Logger;

public class Runner {

	public static void main(String[] args) throws YahooException {
		
		
		Logger logger = Logger.getLogger(StockMarketMonitor.class.getName());  
	    FileHandler fh;  

	    try {  

	        // This block configure the logger with handler and formatter  
	        fh = new FileHandler("MyLogFile.log");  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	        // the following statement is used to log any messages  
	        logger.info("My first log");  

	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
	//    logger.setUseParentHandlers(false);
	    
	   StockMarketGui smg=new StockMarketGui();
	   StockMarketMonitor smm= new StockMarketMonitor();
	   ControllerStockMarket csm=new ControllerStockMarket(smg, smm);
	   smg.start();
		
	}


}
